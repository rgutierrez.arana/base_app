from core.extensions import base as Base
from app.data import (

    Column,
    ForeignKey,
    Integer,
    String,
    Boolean,
    Float,
    ForeignKey,
    BigInteger,
    TIMESTAMP,
    Enum,
    Sequence,
    BOOLEAN,
    Numeric
)
from sqlalchemy import Identity 
from sqlalchemy.orm import Session

#? User Data for API permitions
from app.data.jefatura import Jefatura
from app.data.gerencia import Gerencia
from app.data.viceprecidencia import Viceprecidencia
from app.data.cargo import Cargo
from app.data.colaborador import Colaborador

from app.data.usuarios import Usuario

from app.data.grupo import Grupo
from app.data.usuario_grupo import UsuarioGrupo

from app.data.grupo_permiso import GrupoPermiso
from app.data.permiso import Permiso
from app.data.usuario_permiso import UsuarioPermiso
from app.data.products import Product
from app.data.products_metrics import ProductsMetrics