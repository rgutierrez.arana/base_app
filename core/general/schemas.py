from pydantic import BaseModel
from sqlalchemy.orm import Session
from typing import List
from datetime import datetime
class OrmModel(BaseModel):
    class Config:
        orm_mode = True


class Success(OrmModel):
    status:str = "Success"
    

class SuccessCreated(Success):
    id:int
    
class SuccessUpdated(Success):
    id:int


class DbUser(OrmModel):
    id:int 
    name:str
    mail:str
    state:str
    access_type:str
    active:bool
    full_name:str



class UserAccess:

    def __init__(self, current_user: DbUser, session: Session , permitions:List[str] = None) -> None:
        self.permitions = permitions or []
        self.user = current_user
        self.db = session
        self.selected_permition = None
        
class CommonNamedBase(OrmModel):
    id:int
    register_date:datetime
    modified_date:datetime
    visible:bool
    active:bool
    name:str
    