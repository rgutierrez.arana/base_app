
from typing import List

from app.data.common import CommonFields


def check_active(*models :List[CommonFields]) -> list:
    active_filter = [model.active == True for model in models]
    return active_filter


def check_visible(*models :List[CommonFields]) -> list:
    visible_filter = [model.visible == True for model in models]
    return visible_filter


def check_active_visible(*models: List[CommonFields])->List:
    model_filter_visible = check_active(*models)
    model_filter_active = check_visible(*models)

    return [*model_filter_active, *model_filter_visible]


def clone_model(model:CommonFields):
    
    # Ensure the model’s data is loaded before copying.
    model.id

    table = model.__table__
    non_pk_columns = [k for k in table.columns.keys() if k not in table.primary_key]
    data = {c: getattr(model, c) for c in non_pk_columns}
    data.pop('id')
    return data