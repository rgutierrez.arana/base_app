from core.extensions import yield_session ,engine
from sqlalchemy import text
import sys
import subprocess as sp
import time 
ARGS = ["alembic" , "upgrade" , "head"]
ALEMBIC_VERSION = "a5cd714d37c4"

async def startup():

    execute_text = text("SELECT version_num FROM alembic_version")
    resp = None
    try :
        resp = engine.execute(execute_text)
        items = [row._asdict() for row in resp]
        
    
    except Exception as ex:
        
        print("Not found current version executing update")
        
        alembic_process = sp.Popen(ARGS ,stdout=sp.PIPE , stderr=sp.PIPE )
        
        response, error = alembic_process.communicate()
        print(response.decode())
        print(error.decode())

        
    finally:
        if not resp:

            resp = engine.execute(execute_text)
            items = [row._asdict() for row in resp]

        if items[0]["version_num"] != ALEMBIC_VERSION:
            print("alembic version desincronized")
            sys.exit(112)
        if len(items) < 1:
            print("Error creating session to DB")
            sys.exit(690)

        print("Validated DB connection")
