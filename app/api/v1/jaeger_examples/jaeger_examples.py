from app.jaeger_service.manager import create_span
from .schemas import *
from fastapi import APIRouter, Depends, Query
from core.general.schemas import Success

from dependencies.user_dependencies import user_token_data, UserAccess, user_db_permitions

from .models import Entry
import asyncio

router = APIRouter()

tags = ["JAEGER"]
router.tags = tags


@router.post("/jaeger-trace", response_model=Success, tags=tags)
async def create_new_user(
    args:Entry,
    name :str = Query(None),
):
    await asyncio.sleep(2)
    root_span = create_span(optional_name="Jaeger-trace")
    # raise Exception("Error intero")
    with root_span as current_span:
        current_span.add_event(name="inside span" , attributes=args.dict())
        await asyncio.sleep(2)
        
        
    print(args , name)
    return Success()
