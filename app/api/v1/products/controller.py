

from app.api.v1.products.storages import update_searched_product
from core.extensions import yield_session
from .schemas import ListProduct, NewProduct, ProductMetricOut, SearchProduct, UpdatedProduct
from fastapi import APIRouter, Depends
from core.general.schemas import Success, SuccessCreated, SuccessUpdated
from .services import create_new_product, get_product, get_product_metrics_bs, notify_admins_delete, notify_admins_udpate, update_product_bs
from dependencies.user_dependencies import user_token_data, UserAccess, user_db_permitions
from sqlalchemy.orm import Session
from fastapi import BackgroundTasks


tags = ["PRODUCTS"]

router = APIRouter()

DELETE_PRODUCT_UPDATE = UpdatedProduct(visible=False, active=False)

@router.get("", response_model=ListProduct, tags=tags)
def get_product_filter(
    args: SearchProduct = Depends(SearchProduct),
    
    db:Session  = Depends(yield_session)
):
    products = get_product(db=db,
                           id_product=args.id_product,
                           product_name=args.product_name)
    
    ids_products = [row.id for row in products]
    
    update_searched_product(db =db , id_products=ids_products)
    db.commit()
    return {"products": products}


@router.post("", response_model=SuccessCreated, tags=tags)
def create_product(
    args: NewProduct,
    user_access: UserAccess = user_db_permitions(
        permitions=["PRODUCT_CREATE"]),
) -> int:

    new_product_id = create_new_product(db=user_access.db, args=args)
    user_access.db.commit()
    return SuccessCreated(id=new_product_id)


@router.put("{product_id}", response_model=SuccessUpdated, tags=tags)
def update_product(
    product_id: int,
    args: UpdatedProduct,
    bg_task: BackgroundTasks,
    user_access: UserAccess = user_db_permitions(permitions=["PRODUCT_UPDATE"]),
):
    args.active = None
    args.visible = None

    old_product, updated_product = update_product_bs(db=user_access.db,
                                                     args=args,
                                                     id_product=product_id)

    user_access.db.commit()

    bg_task.add_task(notify_admins_udpate,
                     current_user=user_access.user,
                     old_product=old_product,
                     new_product=updated_product
                     )


    return SuccessCreated(id=updated_product.id)


@router.delete("{product_id}", response_model=SuccessUpdated, tags=tags)
def delete_product(
    product_id: int,
    bg_task : BackgroundTasks ,
    user_access: UserAccess = user_db_permitions(permitions=["PRODUCT_UPDATE"]),
):

    _, updated_product = update_product_bs(db=user_access.db,
                                           args=DELETE_PRODUCT_UPDATE,
                                           id_product=product_id)

    user_access.db.commit()

    bg_task.add_task(notify_admins_delete,
                     current_user=user_access.user,
                     product_info=updated_product,
                     )

    return SuccessCreated(id=updated_product.id)

@router.get("{product_id}" , response_model=ProductMetricOut , tags=tags)
def get_product_metrics(
    product_id:int,
    user_access: UserAccess = user_db_permitions(permitions=["PRODUCT_SEARCH"]),
):
    product_metrics_out = get_product_metrics_bs(db=user_access.db,
                                                 product_id=product_id)
    return product_metrics_out