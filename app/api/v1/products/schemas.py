from decimal import Decimal
from typing import List, Optional
from pydantic import validator
from fastapi import Query,Body,Form,File,UploadFile
from dataclasses import dataclass

from sqlalchemy import Float
from core.general.schemas import CommonNamedBase, OrmModel
from core.general.pydantic_types import Money , GeneratedMoney
@dataclass
class SearchProduct:
    id_product: int = Query(None)
    product_name: str = Query(None)


class SchemaProducts(CommonNamedBase):
    price:Money
    
class ListProduct(OrmModel):
    products :List[SchemaProducts] = []
    
    
class NewProduct(OrmModel):
    name:str 
    price:GeneratedMoney
    


class UpdatedProduct(OrmModel):
    name:Optional[str]
    price:Optional[Money]
    
    visible:Optional[bool]
    active:Optional[bool]
    
class ProductMetricOut(OrmModel):
    id:int
    name:str
    times_queried:int