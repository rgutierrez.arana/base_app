from fastapi import HTTPException , status


not_found_product_exception= HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
    detail="The product provided was not found or is deleted",
    
)

not_found_product_metric_exception = HTTPException(
    status_code=status.HTTP_404_NOT_FOUND,
    detail="The product metric provided was not found or is deleted",

)
