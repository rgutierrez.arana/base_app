from typing import  List, Optional, Tuple
from app.api.v1.products.schemas import NewProduct, UpdatedProduct
from core.dbsetup import  Session

from core.dbsetup import Product , ProductsMetrics
from commons.db_commons import  check_active_visible, clone_model 
from .exceptions import not_found_product_exception , not_found_product_metric_exception

common_product_filter = check_active_visible(Product)
common_product_metric_filter = check_active_visible(ProductsMetrics)


def get_product_db(db: Session,
                   id_product: Optional[int] = None,
                   product_name: Optional[str] = None
)->List[Product]:
    
    queried_products: List[Product] = Product.find_by_id_name(
        db=db,
        current_id=id_product,
        current_name=product_name)
    
    return queried_products

def create_new_product_db(
    db:Session,
    args:NewProduct,
)->int:
    new_product=Product(**args.dict())
    db.add(new_product)
    db.flush()
    
    return new_product.id

def update_product_db(
    db:Session,
    args:UpdatedProduct,
    id_product:int 
)->Tuple[UpdatedProduct , Product]:
    
    product: Product = Product.find_by_id_name(
        db=db,
        current_id=id_product,
        first=True)
    if not product:
        raise not_found_product_exception
    old_product=UpdatedProduct.from_orm(product)
    
    updated_fields = args.dict(exclude_unset=True , exclude_none=True)
    for key , val in updated_fields.items():
        setattr(product , key , val)
    db.flush()
    
    return  old_product , product
    
def update_searched_product(db: Session ,
                            
                            id_products : List[int] = [],
):
        
    for id_product in id_products:    
        products_metrict :ProductsMetrics= db.query(
            ProductsMetrics
        ).filter(
            ProductsMetrics.active == True,
            ProductsMetrics.visible == True,
            ProductsMetrics.id_product == id_product,
            
        ).first()
        
        if not products_metrict:
            
            new_products_metric=ProductsMetrics(
                id_product=id_product,
                
                times_queried=1
            )
            db.add(new_products_metric)
            db.flush()
        else:
            products_metrict.times_queried = products_metrict.times_queried+1
            db.flush()
    
            
def search_product_metrics(db:Session,product_id:int):
    
    product_metric = db.query(ProductsMetrics).filter(
        *common_product_metric_filter,
        *common_product_filter,
        ProductsMetrics.id == product_id
    ).join(
        Product , 
        Product.id == ProductsMetrics.id_product
    ).with_entities(
        Product.id,
        Product.name,
        ProductsMetrics.times_queried
    ).first()
    
    if not product_metric:
        raise not_found_product_metric_exception
    return product_metric