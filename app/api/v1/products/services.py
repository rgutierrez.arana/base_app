from typing import Optional, Tuple
from app.api.v1.products.schemas import NewProduct, UpdatedProduct
from app.api.v1.users.services import get_user_admins

from app.data.products import Product
from app.data.usuarios import Usuario
from app.mailer.sendmail import send_mail_notification
from core.extensions import yield_session
from core.general.schemas import DbUser
from .schemas import ProductMetricOut
from .storages import create_new_product_db, get_product_db, search_product_metrics, update_product_db
from typing import List
from pub_sub.publishers import publisher

from core.dbsetup import  Session

NOTIFICATION_TEMPLATE = "update_product.jinja"
DELETE_TEMPLATE = "delete_product.jinja"

def get_product(db:Session , id_product:Optional[int] =None, product_name:Optional[str] = None):
    queried_products: List[Product] = Product.find_by_id_name(
        db=db,
        current_id=id_product,
        current_name=product_name)
    
    return queried_products

def create_new_product(db:Session , args:NewProduct)->int:
    new_product_id = create_new_product_db(db=db, args=args)
    return new_product_id


def update_product_bs(
    db: Session,
    args: UpdatedProduct,
    id_product: int
) -> Tuple[UpdatedProduct, Product]:
    old_product, updated_product = update_product_db(db=db,
                                                     args=args,
                                                     id_product=id_product)
    

    return old_product, updated_product


def notify_admins_udpate(current_user: DbUser,
                         old_product: UpdatedProduct,
                         new_product: Product,
                         db: Session = None,
                         **kwargs):
    
    if not db:
        temp_db = yield_session()
        db = next(temp_db)
            
    current_admins = get_user_admins(db=db)
    subject = f"Updated Product #{new_product.id} : {new_product.name}"
    
    args = {
        "name" : current_user.name,
        "user_mail" : current_user.mail,
        "product_id" : new_product.id,
        "old_name" : old_product.name,
        "new_name" : new_product.name,
        "old_price" : old_product.price,
        "new_price" : new_product.price,
        "modified_time":  str(new_product.modified_date)[:20],
        "subject" : subject
    }
    send_mail_notification(
        target_users = current_admins,
        template = NOTIFICATION_TEMPLATE,
        args = args,
        subject=subject
    )
    print ( "Sending data to publisher")
    
    publisher.publish_message(
        data=args
    )



def notify_admins_delete(current_user: DbUser,
                         product_info: Product,
                         db: Session = None):

    if not db:
        temp_db = yield_session()
        db = next(temp_db)
        
    current_admins = get_user_admins(db=db)
    subject = f"DELETED Product #{product_info.id} : {product_info.name}"
    
    args = {
        "name" : current_user.name,
        "user_mail" : current_user.mail,
        "product_id" : product_info.id,
        "modified_time":  str(product_info.modified_date)[:20],
        "product_name" : product_info.name
    }
    send_mail_notification(
        target_users = current_admins,
        template = DELETE_TEMPLATE,
        args = args,
        subject=subject
    )
    publisher.publish_message(
        data=args
    )


def get_product_metrics_bs(
    db: Session,
    product_id: int
)->ProductMetricOut:
    product_metrics = search_product_metrics(
        db=db,
        product_id=product_id,
    )
    product_metrics = ProductMetricOut.from_orm(product_metrics)
    return product_metrics
