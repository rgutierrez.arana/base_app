from fastapi import FastAPI
from app.documentation.documentation import generar_ext_documentation
from pub_sub.publishers import storage

products_base_path = "/products"
product_app = FastAPI(
    docs_url=None,
    redoc_url=None,
    title=f"{products_base_path.upper()} API V1.0",
    openapi_url="/swagger/openapi.json",
    swagger_ui_oauth2_redirect_url="/swagger/docs/oauth2-redirect")

prefix = "product"

@product_app.on_event("shutdown")
def close_connection():
    storage.close()
    
    
from app.api.v1.products.controller import router as product_router
product_app.include_router(product_router, prefix=f"/{prefix}")
generar_ext_documentation(app=product_app, base=products_base_path)
