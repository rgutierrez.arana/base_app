# write you database models in this file
from typing import List, Optional , Union
from app.data import (Column,
                      BigInteger,
                      String,
                      Boolean,
                      TIMESTAMP,
                      Identity)

from datetime import datetime

from sqlalchemy.orm import Session




class CommonFields:
    id = Column(BigInteger,Identity(start=1, cycle=True), primary_key=True , autoincrement = True)
    register_date = Column(TIMESTAMP, nullable=False, default=datetime.now)
    modified_date = Column(TIMESTAMP, nullable=False, default=datetime.now, onupdate=datetime.now)
    visible = Column(Boolean, server_default="true", nullable=False)
    active = Column(Boolean, server_default="true", nullable=False)
    
class BaseNamedCommonFields(CommonFields):
    name = Column(String(96), nullable=False, unique=False)


class NamedCommonFields(BaseNamedCommonFields):
    name = Column(String(96), nullable=False, unique=False)

    @classmethod
    def find_by_id_name(cls,
                        db: Session,
                        current_id: Optional[int] = None,
                        current_name: Optional[str] = None,
                        first: bool = False
                        ) -> Union[List[BaseNamedCommonFields], BaseNamedCommonFields]:
        from commons.db_commons import check_active_visible
        common_local_filter = check_active_visible(cls)
        current_query = db.query(cls).filter(*common_local_filter)

        if current_id:
            current_query = current_query.filter(cls.id == current_id)
        elif current_name:
            current_query = current_query.filter(
                cls.name.ilike(f"%{current_name}%"))
        if first:
            only_one: BaseNamedCommonFields = current_query.first()
            return only_one
        multiple: List[NamedCommonFields] = current_query.all()
        return multiple
    