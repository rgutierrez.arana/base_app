
TO RUN THE PROJECT
Install docker locally 
Windows:
https://docs.docker.com/desktop/install/windows-install/
Ubuntu:
https://docs.docker.com/desktop/install/linux-install/

### Docker usage ###
docker-compose build
docker-compose up

default User : admin
default Password : admin

BASE DOCUMENTATION 
- http://localhost:8000/swagger/docs#/

PRODUCT DOCUMENTATION
- http://localhost:8000/products/swagger/docs#

MAIL WEB PAGE (MAILHOG)
- http://localhost:8025/

### RUN SUBSCRIBER ###
You need to have python with redis dependency installed
cd pub_sub
py subscriber.py 


