from fastapi import FastAPI ,APIRouter



class FastAPICustom(FastAPI):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

    def include_router(self, router: APIRouter = None, *args, **kwargs):
        print(f"\n======================= ROUTES {router.tags} ===========================")
        
        for route in router.routes:
            print(f"{kwargs['prefix']}{route.path}")
        print("\n======================= ROUTES ===========================")

        super().include_router(router=router,
                               *args,
                               **kwargs)
