# from pub_sub.storages.redis_storage import RedisStorage
from ..redis_storage import RedisStorage
import json
import redis
from typing import List
from time import sleep

class RedisPublisher:
    def __init__(self, storage: RedisStorage, topic_name: str) -> None:
        if not storage.connection:
            storage.connect()

        self.topic_name = topic_name
        self.storage_connection: redis.Redis = storage.connection

    def publish_message(self, data: dict):
        stringed_data = json.dumps(data)
        print(f"{self.topic_name} -> {stringed_data}")
        self.storage_connection.publish(
            channel=self.topic_name, message=stringed_data)


class RedisSubscriber:
    def __init__(self, storage: RedisStorage, topic_name: str) -> None:
        if not storage.connection:
            storage.connect()
        self.topic_name = topic_name
        self.storage_connection: redis.Redis = storage.connection
        self.subcriber = self.storage_connection.pubsub()
        self.subcriber.subscribe(self.topic_name)

    def listen(self):

        for message in self.subcriber.listen():
            if message:
                if "data" in message:

                    try:
                        data = json.loads(message["data"])
                        yield data
                    except:
                        print("No current data ATM")

class RedisQueue:
    def __init__(self, storage: RedisStorage, name: str) -> None:
        if not storage.connection:
            storage.connect()
            
        self.storage = storage
        self.name = name
        self.storage_connection:redis.Redis = storage.connection
        print(f"connected to redis DB {self.storage_connection}")

    def enque(self, data: dict)->None:
        json_data = json.dumps(data)
        print (f"enquing {self.name} -> sending {json_data}")
        self.storage_connection.lpush(self.name, json_data)
    
    def enque_multiple (self, messages : List[dict]):
        json_data = [json.dumps(message) for message in messages]
        self.storage_connection.lpush(self.name, *json_data)
        
    def deque(self) -> dict:
        dequed_messages: List[str] = self.storage_connection.rpop(self.name)
        if not dequed_messages:
            return None
        dequed_messages = json.loads(dequed_messages)
        return dequed_messages

    def deque_all(self) -> List[dict]:
        dequed_elements = []
        while True:
            dequed_element = self.deque()
            sleep(0.7)
            if not dequed_element:
                break
            print(dequed_element)
            dequed_elements.append(dequed_element)
        return dequed_elements
        